# SportResults tech test demo app

## Application info:
Given an API endpoint retrieves data as a JSON string and converts it into objects in order to filter, order and display them based on given criterias

- minimum iOS version: 13.0
- external dependancies:
-- cocoapods
-- AlamoFire
---
## To install:
Clone this repository (or unzip the archive), run `pod install` on the project directory, then `open Sport\ Results.xcworkspace`

---
## Navigating the application:
To start fetching the data tap the "Get Results" button, an activity indicator spinner will be visible to show the user the app is downloading and parsing data from the API. 
When the data is ready for consumption the activity indicator will stop and hide, as well as the "Get Results" button, and the retrieved data will be visible in a tableview. 
The data will be filtered to display only results from the same day, as per specifications, however if a date is not selected (i.e. when the table is firstly shown) the most recent date with result will be selected as filter. 
To change the filter date tap the "Results for _selected date_" button on top of the table: a modal dialog will present a choice of dates for which the application has retrieved results. 
When the desired date has been selected tapping the "Confirm" button will dismiss the modal dialoog and reload the table for the newly selected date.
To trigger a new data request to the API tap the button "Fetch again" on the date selector modal dialog, or pull the tableview for using the 'pull to refresh' functionality.

---
## Where to go from here:
- Data keeping: Implementing a data persistancy layer (using CoreData, Realm, or other solutions) would save the user from having to fetch the data each time the app is open
- Ipad support: to port the app on iPad platform (and macOs using Catalyst) a new UX would be needed, in order to fully take advantage of the added screen availability and difference in design gudelines
- Detailed data: tapping on a table cell could bring up further details about the single result (where available)
- UX + UI refining: having the application load the data from API (or the persistancy layer) on launch would save the user a tap on the "Get Results" button, shortening their journey to data consumption. Also the app could benefit from a general UI makeover: restyling the tableview, buttons, date selector and the launch screen

