//
//  Date+Extensions.swift
//  Sport Results
//
//  Created by Federico Brandani on 30/09/2022.
//

import Foundation

extension Date {
    static func decodeFromString(_ dateString: String, format: String = "MMM dd, yyyy hh:mm:ss a") -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString) ?? nil
    }
    
    static func fromComponents(year: Int, month: Int, day: Int) -> Date {
        let components = DateComponents(year: year, month: month, day: day)
        return Calendar.current.date(from: components)!
    }

    func getComponent(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
            return calendar.component(component, from: self)
    }
    func toString(format: String = "yyyy-MM-dd") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
