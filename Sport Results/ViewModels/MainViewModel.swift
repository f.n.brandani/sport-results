//
//  MainViewModel.swift
//  Sport Results
//
//  Created by Federico Brandani on 29/09/2022.
//

import UIKit
protocol MainViewProtocol: AnyObject {
    var onNewResultsArray: (() -> Void)! { get }
    var resultsOrder: [Int: SportResultType] { get }
    var resultsArray: [SportResultType: [SportResultObjectProtocol]] { get }
    var filteredResults: [SportResultType: [SportResultObjectProtocol]] { get }
    var dateSelected: Date? { get }
    func loadResults()
    func filterResults(byDate: Date)
}

class MainViewModel: MainViewProtocol {

    var onNewResultsArray: (() -> Void)!
    var resultsArray: [SportResultType: [SportResultObjectProtocol]] = [:]
    var filteredResults: [SportResultType : [SportResultObjectProtocol]] = [:]
    var resultsOrder: [Int: SportResultType] = [
        0: .f1,
        1: .tennis,
        2: .basket_nba
    ]
    var dateSelected: Date?
    lazy var datesWithResults: [Date] = {
        guard !resultsArray.isEmpty else { return [] }
        return Array(Set(
            resultsArray.map { (key: SportResultType, value: [SportResultObjectProtocol]) in
                value.compactMap {
                    Date.fromComponents(year: $0.publicationDateObject.getComponent(.year),
                                        month: $0.publicationDateObject.getComponent(.month),
                                        day: $0.publicationDateObject.getComponent(.day))
                }
            }.flatMap{$0})).sorted(by: {$0.compare($1) == .orderedDescending})
    }()
    
    func loadResults() {
        APIConnector.fetchSportResults { results in
            self.resultsArray = results
            guard let recentDate = self.datesWithResults.first else {
                self.onNewResultsArray()
                return
            }
            self.filterResults(byDate: recentDate)
            self.onNewResultsArray()
        }
    }
    
    func filterResults(byDate: Date) {
        dateSelected = byDate
        filteredResults = filterAndOrderResults(filterByDate: dateSelected!)
        onNewResultsArray()
    }
    
    private func filterAndOrderResults(filterByDate: Date = Date()) -> [SportResultType: [SportResultObjectProtocol]] {
        guard !resultsArray.isEmpty else { return [:] }
        var sortingArray : [SportResultType: [SportResultObjectProtocol]] = [:]
        for sport in resultsArray {
            sortingArray[sport.key] = sport.value.filter({
                ($0.publicationDateObject.getComponent(.day) == filterByDate.getComponent(.day)) &&
                ($0.publicationDateObject.getComponent(.month) == filterByDate.getComponent(.month)) &&
                ($0.publicationDateObject.getComponent(.year) == filterByDate.getComponent(.year))
            }).sorted(by: {
                $0.publicationDateObject.compare($1.publicationDateObject) == .orderedDescending
            })
        }
        return sortingArray
    }
    
}
