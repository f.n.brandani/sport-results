//
//  MainViewController.swift
//  Sport Results
//
//  Created by Federico Brandani on 29/09/2022.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet weak var btn_dateSelector: UIButton!
    @IBOutlet weak var btn_getResults: UIButton!
    @IBOutlet weak var tbl_results: UITableView!
    @IBOutlet weak var activityIndicator_spinner: UIActivityIndicatorView!
    
    let toDateSelectorSegueIdentifier = "mainToDateSelector"
    let pullToRefreshControl = UIRefreshControl()
    
    lazy var viewModel = MainViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.onNewResultsArray = { [unowned self] in
            tbl_results.reloadData()
            updateViews()
        }
        
        tbl_results.delegate = self
        tbl_results.dataSource = self
        
        pullToRefreshControl.attributedTitle = NSAttributedString(string: "Pull to fetch new results")
        pullToRefreshControl.addTarget(self, action: #selector(self.refreshView), for: .valueChanged)
        tbl_results.addSubview(pullToRefreshControl)
    }
    
    @IBAction func btn_getResultsDidTap(sender: UIButton) {
        btn_getResults.isHidden = true
        activityIndicator_spinner.startAnimating()
        refreshView()
    }
    
    @IBAction func btn_deteSelectorDidTap(sender: UIButton) {
        performSegue(withIdentifier: toDateSelectorSegueIdentifier, sender: nil)
    }
    
    @objc func refreshView() {
        viewModel.loadResults()
        updateViews()
    }
    
    func updateViews() {
        pullToRefreshControl.endRefreshing()
        activityIndicator_spinner.stopAnimating()
        let resultsAvailable = !viewModel.resultsArray.isEmpty
        btn_dateSelector.setTitle(titleForDateSelector(date: viewModel.dateSelected), for: .normal)
        btn_dateSelector.isHidden = !resultsAvailable
        tbl_results.isHidden = !resultsAvailable
    }
    
    private func titleForDateSelector(date: Date?) -> String {
        guard let selectedDate = date else {
            return "Tap to select a date"
        }
        let leadingString = "Results for "
        return leadingString.appending(selectedDate.toString())
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == toDateSelectorSegueIdentifier,
            let destinationVC: DateSelectorViewController = segue.destination as? DateSelectorViewController else {return}
        destinationVC.availableDates = viewModel.datesWithResults
        destinationVC.dateSelectedCallback = { date in
            self.viewModel.filterResults(byDate: date)
        }
        destinationVC.fetchAgainCallback = {
            self.viewModel.loadResults()
        }
    }
}

// MARK: - Table management
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.resultsOrder[section]?.rawValue
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.filteredResults.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let thisKey = viewModel.resultsOrder[section], let thisSection = viewModel.filteredResults[thisKey] else { return 0 }
        return thisSection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let thisCell = tableView.dequeueReusableCell(withIdentifier: "sportResultCell") as? SportResultCell,
              let thisKey: SportResultType = viewModel.resultsOrder[indexPath.section],
              let thisSection = viewModel.filteredResults[thisKey] else {
            debugPrint("error in translating result into table view cell")
            return UITableViewCell()
        }
        let thisResult: SportResultObjectProtocol = thisSection[indexPath.row]
        thisCell.lbl_title.text = thisResult.getFormattedOutcome()
        thisCell.lbl_subtitle.text = thisResult.publicationDate
        let backgroundImageName = SportResultCell.backgroundImageName(rawValue: thisKey.rawValue) ?? SportResultCell.backgroundImageName.undetermined
        thisCell.setBackgroundImage(withName: backgroundImageName)
        
        return thisCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
}



class SportResultCell: UITableViewCell {
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var img_backgroundImageView: UIImageView!
    
    func setBackgroundImage(withName name: backgroundImageName) {
        img_backgroundImageView.image = UIImage(named: name.rawValue)
    }
    
    enum backgroundImageName: String {
        case Tennis
        case nbaResults
        case f1Results
        case undetermined = "defaultResultImage"
    }
}
