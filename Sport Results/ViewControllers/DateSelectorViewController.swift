//
//  DateSelectorViewController.swift
//  Sport Results
//
//  Created by Federico Brandani on 30/09/2022.
//

import UIKit

class DateSelectorViewController: UIViewController {
    @IBOutlet weak var lbl_info: UILabel!
    @IBOutlet weak var picker_dateSelector: UIPickerView!
    @IBOutlet weak var btn_confirm: UIButton!
    @IBOutlet weak var btn_fetchAgain: UIButton!
    
    var fetchAgainCallback: (() -> Void)?
    var dateSelectedCallback: ((Date) -> Void)?
    var availableDates: [Date] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_info.text = "Please select a date to display the relative sport results"
        picker_dateSelector.delegate = self
        picker_dateSelector.dataSource = self
    }
    
    @IBAction func btn_confirmDidTap(sender: UIButton) {
        dismiss(animated: true) { [unowned self] in
            guard let callback = self.dateSelectedCallback else { return }
            callback(availableDates[self.picker_dateSelector.selectedRow(inComponent: 0)])
        }
    }
    @IBAction func btn_fetchAgainDidTap(sender: UIButton) {
        dismiss(animated: true) { [unowned self] in
            guard let callback = self.fetchAgainCallback else {return}
            callback()
        }
    }
    
}


extension DateSelectorViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int { 1 }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int { availableDates.count }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let thisDate = availableDates[row]
        return thisDate.toString()
    }
    
    
}
