//
//  SportResultsModel.swift
//  Sport Results
//
//  Created by Federico Brandani on 29/09/2022.
//

import Foundation

enum SportResultType: String, CaseIterable {
    case f1 = "f1Results"
    case tennis = "Tennis"
    case basket_nba = "nbaResults"
}

protocol SportResultObjectProtocol: Codable {
    var publicationDate: String {get}
    var publicationDateObject: Date { get }
    func getFormattedOutcome() -> String
    static func parseFromArray(_ resultsArray: [Dictionary<AnyHashable, Any>]) -> [SportResultObjectProtocol]
}
