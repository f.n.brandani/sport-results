//
//  TennisResultsModel.swift
//  Sport Results
//
//  Created by Federico Brandani on 29/09/2022.
//

import Foundation

struct TennisResultsModel {
    
    /*
    {
        "looser": "Schwartzman ",
        "numberOfSets": 3,
        "publicationDate": "May 9, 2020 11:15:15 PM",
        "tournament": "Roland Garros",
        "winner": "Rafael Nadal"
    },
    */
    
    let publicationDate: String
    let numberOfSets: Int
    let tournament: String
    let winner: String
    let looser: String
}

extension TennisResultsModel: SportResultObjectProtocol {
    var publicationDateObject: Date {
        Date.decodeFromString(publicationDate) ?? Date()
    }

    static func parseFromArray(_ resultsArray: [Dictionary<AnyHashable, Any>]) -> [SportResultObjectProtocol] {
        do {
            let json = try JSONSerialization.data(withJSONObject: resultsArray)
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .useDefaultKeys
            return try decoder.decode([TennisResultsModel].self, from: json)
        } catch {
            print(error)
        }
        return []
    }
    
    func getFormattedOutcome() -> String {
        /// Expected format:
        /// Roland Garros: Novak Djokovic wins against Schwartzman in 5 sets
        "\(tournament): \(winner) wins against \(looser) in \(numberOfSets) sets"
    }
}

// extension for testing purposes
extension TennisResultsModel {
    static func stub(_ stubs: Int) -> [TennisResultsModel] {
        var stubsArray: [TennisResultsModel] = []
        for _ in 0...stubs {
            stubsArray.append(TennisResultsModel(publicationDate: "May 9, 2020 2:00:40 PM",
                                                 numberOfSets: 3,
                                                 tournament: "Roland Garros",
                                                 winner: "Andree Agassi",
                                                 looser: "Serena Williams"))
        }
        return stubsArray
    }
}
