//
//  F1ResultsModel.swift
//  Sport Results
//
//  Created by Federico Brandani on 29/09/2022.
//

import Foundation

struct F1ResultsModel {
    
    /*
    DATASTRUCTURE FROM GETSANDBOX
    
     "publicationDate": "May 9, 2020 8:09:03 PM",
     "seconds": 5.856,
     "tournament": "Silverstone Grand Prix",
     "winner": "Lewis Hamilton"
     */
    
    var publicationDate: String
    let seconds: Float
    let tournament: String
    let winner: String
}

extension F1ResultsModel: SportResultObjectProtocol {
    var publicationDateObject: Date {
        Date.decodeFromString(publicationDate) ?? Date()
    }
    
    static func parseFromArray(_ resultsArray: [Dictionary<AnyHashable, Any>]) -> [SportResultObjectProtocol] {
        do {
            let json = try JSONSerialization.data(withJSONObject: resultsArray)
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .useDefaultKeys
            return try decoder.decode([F1ResultsModel].self, from: json)
        } catch {
            print(error)
        }
        return []
    }
    
    func getFormattedOutcome() -> String {
        /// Expected format:
        /// Lewis Hamilton wins Silverstone Grand Prix by 5.856 seconds
        "\(winner) wins \(tournament) by \(seconds) seconds"
    }
    
}

// extension for testing purposes
extension F1ResultsModel {
    static func stub(_ stubs: Int) -> [F1ResultsModel] {
        var stubsArray: [F1ResultsModel] = []
        for _ in 0...stubs {
            stubsArray.append(F1ResultsModel(publicationDate: "May 9, 2020 8:09:03 PM",
                                             seconds: 3.212,
                                             tournament: "Italian GP",
                                             winner: "Micheal Schumacher"))
        }
        return stubsArray
    }
}
