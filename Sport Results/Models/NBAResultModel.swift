//
//  NBAResultModel.swift
//  Sport Results
//
//  Created by Federico Brandani on 29/09/2022.
//

import Foundation

struct NBAResultsModel {
    
    /*
     {
        "gameNumber": 6,
        "looser": "Heat",
        "mvp": "Lebron James",
        "publicationDate": "May 9, 2020 9:15:15 AM",
        "tournament": "NBA playoffs",
        "winner": "Lakers"
     }
    */
    
    let publicationDate: String
    let gameNumber: Int
    let tournament: String
    let winner: String
    let looser: String
    let mvp: String
}

extension NBAResultsModel: SportResultObjectProtocol {
    var publicationDateObject: Date {
        Date.decodeFromString(publicationDate) ?? Date()
    }

    static func parseFromArray(_ resultsArray: [Dictionary<AnyHashable, Any>]) -> [SportResultObjectProtocol] {
        do {
            let json = try JSONSerialization.data(withJSONObject: resultsArray)
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .useDefaultKeys
            return try decoder.decode([NBAResultsModel].self, from: json)
        } catch {
            print(error)
        }
        return []
    }
    
    func getFormattedOutcome() -> String {
        /// Expected format:
        /// Lebron James leads Lakers to game 4 win in the NBA playoffs
        "\(mvp) leads \(winner) to game \(gameNumber) win in the \(tournament)"
    }
}

// extension for testing purposes
extension NBAResultsModel {
    static func stub(_ stubs: Int) -> [NBAResultsModel] {
        var stubsArray: [NBAResultsModel] = []
        for _ in 0...stubs {
            stubsArray.append(NBAResultsModel(publicationDate: "May 9, 2020 9:15:15 AM",
                                              gameNumber: 3, tournament: "NBA playoffs",
                                              winner: "Bulls",
                                              looser: "Hornets",
                                              mvp: "Micheal Jordan"))
        }
        return stubsArray
    }
}
