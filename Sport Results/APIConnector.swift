//
//  APIConnector.swift
//  Sport Results
//
//  Created by Federico Brandani on 29/09/2022.
//

import Foundation
import Alamofire

final internal class APIConnector {
    private init() {}
    
    static func fetchSportResults(_ providerName: SportResultsProviders.ProviderName? = nil,
                                  completionHandler: @escaping ([SportResultType: [SportResultObjectProtocol]]) -> Void) {
        let selectedProviderName = providerName ?? SportResultsProviders.enabled.first!
        guard let selectedProvider = SportResultsProviders.available.first(where: { $0.name == selectedProviderName } ) else {
            debugPrint("Unable to find a sport result provider named \(selectedProviderName.rawValue)")
            return
        }
        
        selectedProvider.fetchResults { response in
            guard let JSONData = response.data,
                  let responseJSON = try? JSONSerialization.jsonObject(with: JSONData, options: .fragmentsAllowed),
                  let responseJSONDictionary = responseJSON as? Dictionary<AnyHashable,Any> else {
                debugPrint("error serializing");
                return
            }
            selectedProvider.parseResults(responseJSONDictionary, completionHandler)
        }
    }
}

internal struct SportResultsProviders {
    enum ProviderName: String {
        case GetSandBox = "GetSandbox"
    }
    
    fileprivate static let enabled: [ProviderName] = [.GetSandBox]
    fileprivate static let available: [SportResultsProvider] = [
        SportResultsProvider(name: .GetSandBox,
                             baseURLString: "https://ancient-wood-1161.getsandbox.com",
                             appendingPath: "/results",
                             requestMethod: .post,
                             port: 443,
                             headers: ["Content-Type": "application/json"],
                             parseResults: { resultsDict, completion in
                                 var thisSportResultsObject: [SportResultType : [SportResultObjectProtocol]] = [:]
                                     for sport in resultsDict {
                                         guard let sportResultsArray = sport.value as? [Dictionary<AnyHashable, Any>],
                                               let sportKey = sport.key as? String else { continue }
                                         switch sportKey {
                                         case SportResultType.f1.rawValue:
                                             thisSportResultsObject[SportResultType.f1] = F1ResultsModel.parseFromArray(sportResultsArray)
                                         case SportResultType.basket_nba.rawValue:
                                             thisSportResultsObject[SportResultType.basket_nba] = NBAResultsModel.parseFromArray(sportResultsArray)
                                         case SportResultType.tennis.rawValue:
                                             thisSportResultsObject[SportResultType.tennis] = TennisResultsModel.parseFromArray(sportResultsArray)
                                         default:
                                             continue
                                         }
                                     }
                                     completion(thisSportResultsObject)
                             })
    ]
    
}

fileprivate struct SportResultsProvider {
    let name: SportResultsProviders.ProviderName
    let baseURLString: String
    let appendingPath: String
    let requestMethod: HTTPMethod
    let port: Int
    let parameters: [String: Any]? = nil
    let headers: HTTPHeaders?
    
    func fetchResults(completion: @escaping (AFDataResponse<Data>) -> Void) {
        guard let requestURL = URL(string: "\(baseURLString):\(port)\(appendingPath)") else {
            print("error in resolving URL \(baseURLString)\(appendingPath)")
            return
        }

        AF.request(requestURL,
                   method: requestMethod,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers, interceptor: nil).responseData(completionHandler: completion)
    }
    
    var parseResults: (Dictionary<AnyHashable,Any>, ([SportResultType: [SportResultObjectProtocol]]) -> Void) -> Void
    
}



